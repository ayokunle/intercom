class Customer {

    String name;
    double latitude;
    double longitude;
    int user_id;
    double distance;

    Customer(int user_id){
        this.user_id = user_id;
    }

    Customer(double distance){
        this.distance = distance;
    }

    Customer(int user_id, String name, double latitude, double longitude) {
        this.user_id = user_id;
        this.name = name;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public int getUserId(){
        return this.user_id;
    }

}

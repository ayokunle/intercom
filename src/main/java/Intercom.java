import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Intercom {

    private final double DUBLIN_LONG = -6.257664;
    private final double DUBLIN_LAT = 53.339428;

    public static void main(String[] args) throws IOException {

        Intercom intercom = new Intercom();

        //read customers from file
        List<Customer> customers = intercom.readFile("customers.txt");

        //get list of customers within 100km
        List<Customer> validCustomers = intercom.filterCustomers(customers);

        //sort customers by user id
        validCustomers = intercom.sortUserId(validCustomers);

        if(validCustomers != null){
            //print user id and names of valid customers
            for (Customer customer : validCustomers) {
                System.out.println(customer.user_id + " - " + customer.name);
            }
        }
    }

    /**
     * Reads file line by line, converts string to JSON
     * @param filename
     * @return List of Customers
     */
    public List<Customer> readFile(String filename) throws IOException {

        List<Customer> customers = new ArrayList<>();
        JSONObject jsonObject;
        Customer customer;

        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(filename));
            String line = reader.readLine();
            while (line != null) {
                jsonObject = new JSONObject(line);
                customer = new Customer(
                        jsonObject.getInt("user_id"),
                        jsonObject.getString("name"),
                        Double.parseDouble(jsonObject.getString("latitude")),
                        Double.parseDouble(jsonObject.getString("longitude")));
                customers.add(customer);
                // read next line
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
        return customers;
    }

    /**
     * Gets the distance between Dublin office and a given location
     * @param longitude
     * @param latitude
     * @return distance
     */
    public double getDistance(double longitude, double latitude){

        double X1 = Math.toRadians(DUBLIN_LAT);
        double Y1 = Math.toRadians(DUBLIN_LONG);

        double X2 = Math.toRadians(latitude);
        double Y2 = Math.toRadians(longitude);

        //get distance between the 2 locations
        double a = Math.sin(X1) * Math.sin(X2);
        double b =  Y1 - Y2;
        double c = Math.cos(X1) * Math.cos(X2) * Math.cos(b);
        double central_angle = Math.acos(a + c);
        double distance = 6372.8 *  central_angle;

        return distance;
    }

    /**
     * Gets Customers within 100km from Dublin office
     * @param customers
     * @return
     */
    public List<Customer> filterCustomers(List<Customer> customers) {
        List<Customer> validCustomers = new ArrayList<>();

        if(customers == null){
            return customers;
        }

        for (Customer customer : customers) {
            if(customer.name != null) {
                customer.distance = getDistance(customer.longitude, customer.latitude);
            }
            if(customer.distance < 100){
                validCustomers.add(customer);
            }
        }

        return validCustomers;
    }

    /**
     * Sorts customers by User ID
     * @param validCustomers
     * @return
     */
    public List<Customer> sortUserId(List<Customer> validCustomers) {
        if(validCustomers == null){
            return validCustomers;
        }
        return validCustomers.stream()
                .sorted(Comparator.comparing(Customer::getUserId))
                .collect(Collectors.toList());
    }

}

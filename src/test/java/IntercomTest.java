import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.*;

class IntercomTest {

    private Intercom intercom;

    @BeforeEach
    public void setUp() {
        intercom = new Intercom();
    }

    @ParameterizedTest
    @ValueSource(strings = {"customers.txt", "testUsers.txt"})
    void readValidFile(String filename){
        List<Customer> customers = null;
        try {
            customers = intercom.readFile(filename);
        }catch(Exception e){
            e.printStackTrace();
        }
        assertNotNull(customers);
    }

    @ParameterizedTest
    @ValueSource(strings = {"customer.txt", "allusers.json", "invalidJSON.txt", ""})
    void readInvalidFile(String filename){
        List<Customer> customers = null;
        try {
            customers = intercom.readFile(filename);
        }catch(Exception e){
            e.printStackTrace();
        }
        assertNull(customers);
    }

    @Test
    void testMultipleDistances() {

        int DELTA = 10;

        double distance = intercom.getDistance(6.043701, 38.898736);
        assertEquals(Math.ceil(distance), Math.ceil(1858.75), DELTA);

        distance = intercom.getDistance(67.9475, 107.85646);
        assertEquals(Math.ceil(distance), Math.ceil(4943.76), DELTA);

        distance = intercom.getDistance(-90.87353, -88.6365);
        assertEquals(Math.ceil(distance), Math.ceil(15921.23), DELTA);

        distance = intercom.getDistance(-359.87353, 0.6365);
        assertEquals(Math.ceil(distance), Math.ceil(5889.61), DELTA);

    }

    @RepeatedTest(5)
    void testfilterCustomers(){
        List<Customer> customers = new ArrayList<>();
        int count = 0;

        for (int i = 0; i < 20; i ++){
            double distance = 0 + (500 - 0) * new Random().nextDouble();

            if(distance < 100) count++;

            customers.add(new Customer(distance));
        }
        customers = intercom.filterCustomers(customers);
        assertEquals(count, customers.size());

        customers = null;
        customers = intercom.filterCustomers(customers);
        assertNull(customers);

        customers = new ArrayList<>();
        customers = intercom.filterCustomers(customers);
        assertEquals(0, customers.size());
    }

    @RepeatedTest(5)
    void testSortUserId(){

        List<Customer> customers = new ArrayList<>();

        for (int i = 0; i < 20; i ++){
            customers.add(new Customer(new Random().nextInt()));
        }
        customers = intercom.sortUserId(customers);
        assertEquals(true, isSorted(customers, Customer::getUserId));

        customers = null;
        customers = intercom.sortUserId(customers);
        assertEquals(true, isSorted(customers, Customer::getUserId));

        customers = new ArrayList<>();
        customers = intercom.sortUserId(customers);
        assertEquals(true, isSorted(customers, Customer::getUserId));

    }

    private static <T, R extends Comparable<? super R>> boolean isSorted(List<T> list, Function<T, R> f) {
        Comparator<T> comp = Comparator.comparing(f);

        if(list == null){
            return true;
        }

        for (int i = 0; i < list.size() - 1; ++i) {
            T left = list.get(i);
            T right = list.get(i + 1);
            if (comp.compare(left, right) >= 0) {
                return false;
            }
        }
        return true;
    }
}
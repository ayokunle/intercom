**Intercom Test**

The program takes in a text file with user information and returns a list of users that are within 100 km of the Dublin office.

It is written in Java and built with Maven. The entry point of the program is Intercom.java

The program can be run with Eclipse or IntelliJ.

The output of the program can be found in output.txt.

The tests were created with JUnit. The test units can be found in IntercomTest.java.

The test results can be seen here - https://pasteboard.co/K5504xMC.png


